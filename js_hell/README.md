# JS personal hell

Why ;-;

## Content

- Web sockets made with nodejs
	- good alternative: https://github.com/typicode/json-server

- Angular examples
	- test some nodejs packages
	- test Router class
	- test Observable class

- React examples
	- simple hello world
	- components example with Form class

### Useful stuff

- https://github.com/websockets/ws#sending-and-receiving-text-data

- https://github.com/HenningM/express-ws

- https://dev.to/sachinsarawgi/basic-http-server-using-nodejs-from-scratch-2p6k

- https://www.digitalocean.com/community/tutorials/how-to-create-a-web-server-in-node-js-with-the-http-module

- https://medium.com/angular-in-depth/how-to-create-a-memory-leak-in-angular-4c583ad78b8b
