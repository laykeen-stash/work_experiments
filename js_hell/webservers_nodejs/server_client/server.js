//source: https://medium.com/hackernoon/implementing-a-websocket-server-with-node-js-d9b78ec5ffa8

const http = require('http');
const static = require('node-static');
const file = new static.Server('./');
const server = http.createServer((req, res) => {
  req.addListener('end', () => file.serve(req, res)).resume();
});
const port = 3210;
server.listen(port, () => console.log(`Server running at http://localhost:${port}`));
