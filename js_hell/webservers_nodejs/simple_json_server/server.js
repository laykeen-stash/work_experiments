const http = require("http");

const host = 'localhost';
const port = 3003;
const response_json = {
                        "code": "0x20",
                        "status": "1",
                        "requestID": "0x20",
                        "requestType": "0x01"//,
                        // "current":"1",
                        // "time":"2020-08-03T15:46:15+02:00",
                        // "setpoint":"Delta",
                        // "0":"0",
                        // "lastReset":"2020-08-03T15:46:15+02:00",
                        // "isProgrammingEnable":"1"
                      };

const requestListener = function (req, res) {
    res.setHeader("Content-Type", "application/x-www-form-urlencoded");
    //change this port for further experiments
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
    res.writeHead(200);
    res.end(`{"message": "This is a JSON response"}`);
    console.log(`Request received!`);
    //console.log(`\nREQUEST: `);
    //console.log(req);
    //console.log(`\nRESPONSE: `);
    //console.log(res);
    //console.log(`\n\n\n\n`);

};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});
