import React from 'react';
// import ReactDOM from 'react-dom';

class LifeAndDead extends React.Component {
    //MOUNTING
    constructor(props) {
        super(props);
        this.state = {favColor: "blue"};
        console.log("constructor called!");
    }

    static getDerivedStateFromProps(props, state) {
        console.log("getDerivedStateFromProps called!");
        return null;
    }

    componentDidMount() {
        setTimeout(
            () => {
                this.setState({favColor: "black"})
            }, 1000);
        console.log("componentDidMount called!");
    }

    render() {
        console.log("render called!");
        return (
            <div>
                <h1>Color: {this.state.favColor}</h1>
                <p id="old"></p>
                <p id="update"></p>
            </div>
        );
    }

    //UPDATING
    getSnapshotBeforeUpdate(prevProps, prevState) {
        document.getElementById('old').innerHTML = "Before update = "+prevState.favColor;
        console.log("getSnapshotBeforeUpdate called!");
        return null;
    }

    componentDidUpdate() {
        document.getElementById('update').innerHTML = "Before update = "+this.state.favColor;
        console.log("componentDidUpdate called!");
    }

    // static shouldComponentUpdate() {
    //     return true;
    // }

    //UNMOUNTING
    componentWillUnmount() {
        console.log("componentWillUnmount called!");
    }
}

export default LifeAndDead;
