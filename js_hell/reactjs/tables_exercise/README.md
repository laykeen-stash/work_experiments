This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Used links and commands

https://www.telerik.com/kendo-react-ui/getting-started/#toc-sales-dashboard-sample-app

```bash
npm install --save @progress/kendo-react-grid @progress/kendo-data-query @progress/kendo-react-inputs @progress/kendo-react-intl @progress/kendo-react-dropdowns @progress/kendo-react-dateinputs @progress/kendo-drawing @progress/kendo-react-data-tools @progress/kendo-react-animation

npm install --save @progress/kendo-theme-bootstrap
```

https://www.telerik.com/kendo-react-ui/components/layout/menu/
