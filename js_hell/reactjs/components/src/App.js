import React from 'react';
import './App.css';
import Form from './Form.js';
import Container from 'react-bootstrap/Container';

function App() {
  return (
    <div className="App">
        <Container className="p-3">
            <header className="App-header">
                <Comment date={comment.date} text={comment.text} author={comment.author}/>
                <Form></Form>
            </header>
        </Container>
    </div>
  );
}

function Comment(props) {
    return (
        <div className="Comment">
            <UserInfo user={props.author} />
            <div className="Comment-text">
                {props.text}
            </div>
            <div className="Comment-date">
                {formatDate(props.date)}
            </div>
        </div>
    );
}

function UserInfo(props) {
    return (
        <div className="UserInfo">
            <Avatar user={props.user} />
            <div className="UserInfo-name">
                {props.user.name}
            </div>
        </div>
    );
}

function Avatar(props) {
    return (
        <img
            className="Avatar"
            src={props.user.avatartUrl}
            alt={props.user.name}
        />
    );
}

function formatDate(date) {
    return date.toLocaleDateString();
}

const comment = {
    date: new Date(),
    text: 'Hello React',
    author:{
        name: 'React',
        avatartUrl: 'https://placekitten.com/g/64/64'
    }
};

export default App;
