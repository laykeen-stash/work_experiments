import React, {Component} from 'react';
import '@progress/kendo-theme-bootstrap/dist/all.css';
import './App.css';
import Menu from './Components/Menu/Menu.js';

class App extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="App">
                <Menu/>
            </div>
        );
    }
}

export default App;
