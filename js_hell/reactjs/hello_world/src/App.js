import React from 'react';
import logo from './logo.svg';
import './App.css';

//experiments
const hello_element = <h1>Hello world!</h1>;
const name = 'World, Hello World';
const name_print = <h2>My name is {name}</h2>;

function getGreeting(user) {
    if(user){
        return <p>Hello {user}!</p>;
    }
    return <p>Hello stranger...</p>;
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {hello_element}
        {name_print}
        <a href="#">{getGreeting(name)}</a>
      </header>
      <footer>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
          </a>
      </footer>
    </div>
  );
}

export default App;
