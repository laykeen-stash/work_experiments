# React experiments

## Requirements

In atom: [babeljs highlight](https://atom.io/packages/language-babel)

Use react locally (stop downloading npm packages every time)

```bash
npm install -g create-react-app
```

## Example

```bash
npx create-react-app hello_world
cd hello_world
npm install react-bootstrap bootstrap
npm run
```

## Stuff

https://www.telerik.com/kendo-react-ui/components/animation/

https://www.telerik.com/kendo-react-ui/getting-started/#toc-adding-the-styles

https://blog.bitsrc.io/how-to-build-a-react-progressive-web-application-pwa-b5b897df2f0a
