let json_response = {"data":{"status":"1"},
                    "requestID":"0xFE",
                    "requestType":"0x01"};

const http = require('http');
const static = require('node-static');
const file = new static.Server('./');
const server = http.createServer((req, res) => {
  req.addListener('end', () => file.serve(req, res)).resume();
});
const port = 3210;
server.listen(port, () => console.log(`Server running at http://localhost:${port}`));
