# TestNodejs

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Dependencies

```bash
npm i ngx-progressbar

npm i jquery

npm install ngx-malihu-scrollbar
```

https://github.com/jfcere/ngx-malihu-scrollbar

https://github.com/MurhafSousli/ngx-gallery/wiki


### update angular cli from 1.0.0 to +10.0.0

```bash
npm install @angular/cli@^6.0.0
ng update @angluar/cli --migrate-only --from=1

ng update --all

npm i -g npm-check-updates
ncu -u
npm install

npm i install-peers
npm install
```

### in case of memory leak

```bash
node --max_old_space_size=8192 node_modules/@angular/cli/bin/ng serve
```
