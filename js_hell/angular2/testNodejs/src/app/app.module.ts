import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgProgressModule } from 'ngx-progressbar';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgProgressModule.withConfig({
      color: "red",
      fixed: false,
      ease: "easeInOutBounce",
      spinner: false,
      meteor: false
    }),
    MalihuScrollbarModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
