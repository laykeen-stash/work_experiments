import { Component, OnDestroy, OnInit } from '@angular/core';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

interface ITheme {
  name: string;
  class: string;
  options: MCustomScrollbar.CustomScrollbarOptions;
}

@Component ({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'testNodejs';

  startedClass = false;
  completedClass = false;

  onStarted() {
    this.startedClass = true;
    setTimeout(() => {
      this.startedClass = false;
    }, 800);
  }

  onCompleted() {
    this.completedClass = true;
    setTimeout(() => {
      this.completedClass = false;
    }, 800);
  }

  themes: ITheme[] = [
    { name: '"minimal-dark"', class: 'dark', options: { axis: 'y', theme: 'dark-thick', scrollButtons: { enable: true } } },
    { name: '"rounded-dots-dark"', class: 'light', options: { axis: 'y', theme: 'rounded-dots-dark', scrollButtons: { enable: true } } },
    { name: '"custom-theme"', class: 'dark', options: { axis: 'y', theme: 'bezzera', scrollbarPosition: 'outside', scrollButtons: { enable: true } } },

  constructor(
    private mScrollbarService: MalihuScrollbarService,
  ) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(document.body, { axis: 'y', theme: '3d-dark', scrollButtons: { enable: true } });
  }

  size: number = 50;
  Arr = Array(this.size).fill("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

}
