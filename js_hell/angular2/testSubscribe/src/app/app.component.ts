import { Component, NgModule, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'testSubscribe';

  ngOnInit() {
      this.foo();
  }

  foo() {
      const foo = new Observable(subscriber => {
          console.log('Hello from the observer');
          subscriber.next(42);
          subscriber.next(44);
          setTimeout(() => {
              subscriber.next(400);
          }, 1000);
      });

      console.log("Ready to print");
      foo.subscribe(
          x => {
              console.log("Hello from x "+x);
          }
      );
      console.log("Ended");
  }
}
