# How to shade some css borders that HAS border-radius

## create the right structure

```html
<div class="box">
	<div class="inner_box">
		<div class="message">Hello</div>
	</div>
	<div class="shadow_box">
		<div class="shadow">&nbsp;</div>
	</div>
</div>
```

## add style

```css
.box {
	z-index: 1;
	background-color: black;
	color: white;
}
.inner_box {
	z-index: 3;
	border-radius: 2px;
	border: 1px solid red;
}
.shadow_box {
	position: fixed;
	z-index: 2;
}
.shadow {
	position: relative;
	background-image: linear-gradient(to right, transparent, black);
	width: 100%;
	height: 100%;
	top: -1px;
}
```