# Work_experiments

Various experiments made at work with no sensitive data on it, only not so functioning functions ;-;


## veeery useful stuff

https://web.dev/defining-core-web-vitals-thresholds/

https://medium.com/angular-in-depth/how-to-create-a-memory-leak-in-angular-4c583ad78b8b

https://itnext.io/angular-rxjs-detecting-memory-leaks-bdd312a070a0

https://blog.bitsrc.io/debugging-memory-leaks-in-angular-4bc7b3eab569

https://leaverou.github.io/css3patterns/

http://standardista.com/cssgradients/

https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Images/Using_CSS_gradients

https://bit.dev/

https://blog.angular.io/angular-tools-for-high-performance-6e10fb9a0f4a

https://hixonrails.com/ruby-on-rails-tutorials/ruby-on-rails-application-monitoring/

http://webtypography.net/intro/

https://rubygems.org/

https://hixonrails.com/ruby-on-rails-tutorials/ruby-on-rails-security-best-practices/

https://easings.net/

https://www.kirupa.com/hodgepodge/cookies_local_session_storage.htm

https://www.kirupa.com/learn/ui_index.htm

https://www.kirupa.com/tricks/dealing_with_dark_mode.htm

https://www.kirupa.com/hodgepodge/ui_virtualization.htm

https://w3c.github.io/uievents/

https://guides.rubyonrails.org/getting_started.html

https://hub.docker.com/r/bitnami/rails

https://codeburst.io/react-js-for-beginners-the-basics-87ef6e54dae7

https://programmingwithmosh.com/css/css-animations-and-transitions/

https://programmingwithmosh.com/react/drag-and-drop-react/

https://codeconvey.com/pure-css-off-canvas-menu/

https://mobyproject.com/

https://deividsdocs.wordpress.com/2020/04/21/sending-fail2ban-notifications-using-a-telegram-bot/

https://github.com/shafiqsaaidin/fail2ban-telegram-notification

https://github.com/opensupports/opensupports

https://css-tricks.com/almanac/properties/f/filter/

https://nanoc.ws/about/

https://css-tricks.com/want-to-get-better-at-code-teach-someone-css/

https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History

https://stackoverflow.com/a/5190323

https://www.atlassian.com/git/tutorials/git-log

https://stackoverflow.com/a/17025745

https://alistapart.com/article/neveruseawarning/

### PWA stuff

https://web.dev/pwa-checklist/

https://resilientwebdesign.com/

https://snugug.com/musings/principles-responsive-web-design/

https://uxdesign.cc/why-you-should-design-the-content-first-for-better-experiences-374f4ba1fe3c

https://alistapart.com/article/content-out-layout/

https://web.dev/customize-install/

https://developers.google.com/web/ilt/pwa/working-with-indexeddb

https://localforage.github.io/localForage/

https://serviceworke.rs/

https://developers.google.com/web/updates/2015/12/background-sync

https://uxdesign.cc/what-you-should-know-about-skeleton-screens-a820c45a571a

https://dev.to/akshaykumar6/progressive-web-apps-custom-splash-screen-ce7

https://appsco.pe/developer/splash-screens

https://developers.google.com/web/fundamentals/push-notifications/permission-ux

https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps

https://github.com/mdn/pwa-examples/tree/master/a2hs

https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API/Introduction

### Thanks reactjs for that regex

regex to accept only localhost IPs:

- /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/

### SVG stuff

https://github.com/svgdotjs/svg.js

https://github.com/twbs/icons

https://github.com/SVG-Edit/svgedit

https://github.com/svgdotjs/svg.js

https://github.com/simple-icons/simple-icons

https://jakearchibald.github.io/svgomg/

https://github.com/badges/shields

https://github.com/produck/svg-captcha

https://github.com/gilbarbara/logos

https://github.com/astrit/css.gg#2-single-icon

https://github.com/btmills/geopattern

https://github.com/RazrFalcon/svgcleaner

https://github.com/leungwensen/svg-icon

https://github.com/marionebl/svg-term-cli

https://github.com/feathericons/feather

https://github.com/shrhdk/text-to-svg


## Really High performance stuff

https://requestmetrics.com/web-performance/performance-profiling-google

https://blogs.akamai.com/2016/02/understanding-brotlis-potential.html
